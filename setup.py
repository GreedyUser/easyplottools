from distutils.core import setup

setup(
    name='easyplottools',
    version='0.1',
    packages=['easyplottools',],
    license='Apache License, Version 2.0',
    author = "Jeff Ward",
    description = ("Simplifies some common plotting tasks."),
    install_requires = ['matplotlib'],
    long_description=open('README.txt').read(),
)