import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from mpl_toolkits.mplot3d import Axes3D
import os

COLOR_MAP = ('blue','red','green','orange','black','yellow','purple')

def checkFile(filename):
    filename = os.path.abspath(filename)
    head, tail = os.path.split(filename)
    #head = os.path.abspath(head)
    if not os.path.exists(head):
        os.makedirs(head)
    return filename

class EasyPlotTools:
    def __init__(self, **kwargs):
        self.figSize = kwargs.get("figSize", (8,4))
        self.colorMap = kwargs.get("colorMap", COLOR_MAP)

    def getColor(self, index):
        index = index % len(self.colorMap)
        return self.colorMap[index]

    def plotAndSave(self, x, x_label, y, y_label, Z_data, fileName):
        """

        :param x:
        :param x_label:
        :param y:
        :param y_label:
        :param Z_data: list of touple(ZData, label, color)
        :param fileName:
        :return:
        """
        fileName = checkFile(fileName)
        X, Y = np.meshgrid(x,y)
        fig= plt.figure(figsize=self.figSize)
        ax = fig.add_subplot(111, projection='3d')
        for currentColor, data in enumerate(Z_data):
            Z = data[0]
            label = data[1]
            if len(data) ==3:
                color = data[2]
            else:
                color = self.getColor(currentColor)
            print ("X:{} Y:{} Z:{}".format(np.shape(X),np.shape(Y), np.shape(Z)))
            ax.plot_wireframe(X, Y, Z, label=label, color=color)

        ax.legend(loc=2)
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)
        fig.tight_layout()
        fig.savefig(fileName)
        plt.close()
        print (fileName)


    def plot2dSparse(self, layers, labels=None, colors=None, xAxis=None, xLabel=None, yAxis = None, yLabel =None, sortByX=True, legendLoc=2):

        fig = plt.figure(figsize=self.figSize)
        ax = fig.add_subplot(111)
        # fig.x_label(xLabel)
        # fig.y_label(yLabel)

        for i, layer in enumerate(layers):
            if colors is not None:
                color = colors[i]
            else:
                color = self.getColor(i)
            if sortByX:
                layer = sorted(layer)
            layer = np.transpose(layer)
            if not labels is None:
                ax.plot(layer[0], layer[1], label=labels[i], color=color)
            else:
                ax.plot(layer[0], layer[1], color=color)
        if labels is not None:
            ax.legend(loc=legendLoc)
        if xLabel is not None:
            ax.set_xlabel(xLabel)
        if yLabel is not None:
            ax.set_ylabel(yLabel)
        if xAxis is not None:
            ax.set_xlim([min(xAxis), max(xAxis)])
        if yAxis is not None:
            ax.set_ylim([min(yAxis), max(yAxis)])
        fig.tight_layout()
        return fig, ax

    def plotAndSave2dSparse(self, layers, fileName, **kwArgs):
        fileName = checkFile(fileName)
        fig,_ = self.plot2dSparse(layers, **kwArgs)
        fig.savefig(fileName)
        plt.close()
        print (fileName)


    #results is an array of result. Each result has a "parameter" map that has the key/value parameters
    #that were used to generate the result.
    #each result then is a map of key/value pairs of data that were collected for that result.
    def buildXZData(self, results, xvariable, yvariable, dataLabel):
        x = set()
        y = set()
        for result, params in results:
            x.add(params[xvariable])
            y.add(params[yvariable])

        x = sorted(x)
        y = sorted(y)

        zData = [[None for a in range(len(y))] for b in range(len(x))]
        for result, params in results:
            xindex = x.index(params[xvariable])
            yindex = y.index(params[yvariable])
            zData[xindex][yindex] = result[dataLabel]
        # print zData
        return x, y, zData

    def subResults(self, results, parameter):
        subResults = []
        for result, params in results:
            if parameter in params:
                subResults.append((result, params))
        return

    def plotAndSave3dSparse(self, layers, fileName, labels=None, colors=None, xAxis=None, xLabel=None, yAxis = None, yLabel =None, zAxis = None, zLabel =None):
        """ Provides a simpler way to plot surcfaces

        :param layers:
        :param fileName:
        :param xAxis:
        :param xLabel:
        :param yAxis:
        :param yLabel:
        :param zAxis:
        :param zLabel:
        :return:
        """
        fileName = checkFile(fileName)

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        fakeLines = []
        for i, layer in enumerate(layers):
            if colors is not None:
                color = colors[i]
            else:
                color = self.getColor(i)
            layer = np.transpose(layer)
            ax.plot_trisurf(layer[0], layer[1], layer[2], color=color, alpha=0.5)
            fakeLines.append(mpl.lines.Line2D([0], [0], linestyle="none", c=color, marker='o'))
        #Fake labels idea came from: http://stackoverflow.com/questions/27449109/adding-legend-to-a-surface-plot
        if labels is not None:
            ax.legend(fakeLines, labels, numpoints=1)
        if xLabel is not None:
            ax.set_xlabel(xLabel)
        if yLabel is not None:
            ax.set_ylabel(yLabel)
        if zLabel is not None:
            ax.set_zlabel(zLabel)
        if xAxis is not None:
            ax.set_xlim([min(xAxis), max(xAxis)])
        if yAxis is not None:
            ax.set_ylim([min(yAxis), max(yAxis)])
        if zAxis is not None:
            ax.set_zlim([min(zAxis), max(zAxis)])

        fig.tight_layout()
        fig.savefig(fileName)
        plt.close()
        print (fileName)



    def getZData(self, xParameter, yParameter, additionalParameters, dataFunction):
        """Builds up a "z" that can be used to plot on a 2d graph.

        :param xParameter: touple (parameterName, list of values)
        :param yParameter: touple (parameterName, list of values)
        :param additionalParameters: dict of values that will be sent in addition to the x/y parameters
        :param dataFunction: function of the form fn(parameters) where parameters is a dict
        :return: zData
        """
        zData = [[None for a in range(len(yParameter[1]))] for b in range(len(xParameter[1]))]
        for x in xParameter[1]:
            for y in yParameter[1]:
                parameters = {xParameter[0]:x, yParameter[0]:y}
                parameters.update(additionalParameters)
                zData[xParameter[1].index(x)][yParameter[1].index(y)] = dataFunction(parameters)
        return zData

def getAxisData(points):
    axisData = None
    if len(tuple(np.shape(points))) ==2:
        points = [points]
    for p in points:
        for point in p:
            if axisData is None:
                axisData = [set() for i in range(len(point))]
            for i, val in enumerate(point):
                axisData[i].add(val)
    return tuple(sorted(data) for data in axisData)


