"""
Copyright 2017 Jeff Ward

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, 
software distributed under the License is distributed on an 
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
either express or implied. See the License for the specific 
language governing permissions and limitations under the License.
"""

import easyplottools as ept

sparceData3D1 = (
(1, 1, 2), (1, 3, 4), (3, 6, 9), (6, 3, 9), (2, 6, 8), (1, 6, 7), (6, 6, 12), (10, 3, 13), (1, 9, 10), (9, 1, 10),
(9, 19, 28))
sparceData3D2 = ((10, 10, 10), (0, 10, 10), (10, 0, 10), (0, 0, 10))

ept.plotAndSave3dSparse((sparceData3D1, sparceData3D2), "results/example3da", labels=('1', '2'), colors=('r', 'g'),
                        xLabel="x", yLabel="y", zLabel="z", zAxis=[1, 25])
ept.plotAndSave3dSparse((sparceData3D2,), "results/example3db.pdf")

data2D1 = ((1, 5), (5, 3), (4, 2))
data2D2 = ((1, 6), (5, 4), (4, 4), (8, 2))

ept.plotAndSave2dSparse((data2D1, data2D2), "results/example2da", labels=("1", "2"), legendLoc=2)
ept.plotAndSave2dSparse((data2D1, data2D2), "results/example2db", sortByX=False)

fig, ax = ept.plot2dSparse((data2D1, data2D2))
ax.errorbar((2, 5), (4, 3), yerr=((.1, .2), (.1, .2)), fmt="o")
fig.savefig("results/example2dc.pdf")
fig.clear()
