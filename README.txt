Copyright 2017 Jeff Ward

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
either express or implied. See the License for the specific
language governing permissions and limitations under the License.



BIG WARNING

This is probably best used as an example for how to use some matplotlib functions. It is not intended to be full-featured
and at best allows you to (just slightly) more easily create simple 2-d and 3-d plots.

I may update this as I use it more, but you are safest expecting no further updates. You should also assume there are
large bugs that will never be fixed.

See example.py for usage.